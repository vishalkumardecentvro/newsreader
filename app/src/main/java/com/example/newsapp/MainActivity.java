package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private Retrofit retrofit;
    private RetrofitInterface retrofitInterface;
    private List<Article> articles = new ArrayList<>();
    String country;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private newsAdapter mAdapter;
    private Context context;

    private String BASE_URL = "http://newsapi.org/v2/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;


        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitInterface = retrofit.create(RetrofitInterface.class);

        recyclerView = findViewById(R.id.NewsRecyclerView);
        layoutManager = new LinearLayoutManager(this);


        getCountry();

        data_fetch();


    }

    private void getCountry() {

        Locale locale = Locale.getDefault();
        country = String.valueOf(locale.getCountry());
        country = country.toLowerCase();
    }


    private void data_fetch() {



        String APIkey = "442b5ae7bbe14ec7ad9445f9bc48cbbf";

        Call<News> call = retrofitInterface.executeNews(country,APIkey);

        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                if(response.isSuccessful() && response.body().getArticles() != null)
                {
                    articles = response.body().getArticles();
                    buildRecyclerView();


                }

            }

            @Override
            public void onFailure(Call<News> call, Throwable t) {

                Log.i("error",t.getMessage());
            }
        });



    }





    private void buildRecyclerView() {

        mAdapter = new newsAdapter(articles,context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClick(new newsAdapter.onItemClick() {
            @Override
            public void onItemClick(int position) {
                String content = articles.get(position).getContent();
                String imageUrl = articles.get(position).getUrlToimage();

                Bundle bundle = new Bundle();
                bundle.putString("content",content);
                bundle.putString("url",imageUrl);
                Intent intent = new Intent(getApplicationContext(),ContentSection.class);
                intent.putExtras(bundle);

                startActivity(intent);
            }
        });
    }


}