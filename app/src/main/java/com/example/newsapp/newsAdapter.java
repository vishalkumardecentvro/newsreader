package com.example.newsapp;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class newsAdapter extends RecyclerView.Adapter<newsAdapter.newsViewHolder> {
    private List<Article> newsList;
    private Context mContext;
    private onItemClick mListener;

    public void setOnItemClick(onItemClick listener)
    {
        mListener = listener;
    }

    public interface onItemClick{
        void onItemClick(int position);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

    public class newsViewHolder extends RecyclerView.ViewHolder {
        TextView title,authorAndSource;
        ImageView newsImage;
        CardView newsCard;
        public newsViewHolder(@NonNull View itemView, final onItemClick listener)
        {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            newsCard = itemView.findViewById(R.id.newsCardLayout);
            newsImage = itemView.findViewById(R.id.newsImage);
            authorAndSource = itemView.findViewById(R.id.authorAndSourceTextView);

            newsCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null)
                    {
                        int position = getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                        {

                            listener.onItemClick(position);

                        }
                    }

                }
            });
        }
    }

    @NonNull
    @Override
    public newsAdapter.newsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_cards,null,false);
        newsViewHolder news = new newsViewHolder(v,mListener);
        return news;
    }

    public newsAdapter(List<Article> newsList, Context context) {
        this.newsList = newsList;
        this.mContext = context;

    }

    @Override
    public void onBindViewHolder(@NonNull newsAdapter.newsViewHolder holder, int position) {

        Article currentItem = newsList.get(position);
        holder.title.setText(currentItem.getTitle());
        holder.authorAndSource.setText("Author- "+currentItem.getAuthor()+"\t\t\t");
        holder.authorAndSource.append("Source- "+currentItem.getSource().getName());
        String imageUrl = currentItem.getUrlToimage();
        Picasso.with(mContext).load(imageUrl).fit().into(holder.newsImage);



    }




}
