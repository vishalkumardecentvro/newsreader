package com.example.newsapp;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {


    @GET("top-headlines")
    Call<News> executeNews(@Query("country") String Country,
                           @Query("Apikey") String APIkey
                                       );

}
