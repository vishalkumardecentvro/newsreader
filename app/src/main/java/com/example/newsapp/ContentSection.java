package com.example.newsapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class ContentSection extends AppCompatActivity {

    private ImageView newsImage;
    private TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_section);

        newsImage = findViewById(R.id.ContentImage);
        content = findViewById(R.id.ContentText);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String imageUrl = bundle.getString("url");
        String contentNews = bundle.getString("content");

        if(contentNews == null)
        {
            content.setText("Content is not available for this news");
        }
        else
            content.setText(contentNews);

        Picasso.with(this).load(imageUrl).fit().into(newsImage);



    }
}